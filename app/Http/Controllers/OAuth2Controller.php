<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Http\Requests;

class OAuth2Controller extends Controller
{
	const     baseURL = "https://dev.careerhub.com.au/";
	const     baseURI = "version3/";
	const    clientID = "QUTTest";
	const      secret = "MpaAJPw7+ixPhUI44N2jzqYYK825n67wQ4xnqac2JB0=";
	const redirectUri = "http://localhost/callback";
	const       scope = "JobSeeker.Events";

	public static function getAuthenticationRedirect() {
		return
			self::baseURL .
			self::baseURI .
			'/oauth/auth'.
			'?redirect_uri='.
				self::redirectUri .
			'&response_type='.
				'code'.
			'&client_id=' .
				self::clientID .
			'&scope=' .
				self::scope;
	}

	public static function postRequest($endpoint, $data, $showHttpFails = 'true') {
		$client = new Client([
			'base_uri' => self::baseURL,
			'timeout' => 2.0,
		]);
		$data = array_add($data, 'client_id', self::clientID);
		$data = array_add($data, 'client_secret', self::secret);
		$data = array_add($data, 'redirect_uri', self::redirectUri);
		$requestData = [
			'form_params' => $data,
			'http_errors' => $showHttpFails
		];
		try {
			return $client->request('POST', self::baseURI . $endpoint, $requestData);
		} catch (\Exception $e) {
			abort(500, "Error: " . $e->getMessage());
			return false;
		}
	}

	public static function authorizedGetRequest($user, $endpoint, $showHttpFails = true) {
		$client = new Client([
			'base_uri' => self::baseURL,
			'timeout' => 2.0,
		]);
		try {
			$response =  $client->request('GET', self::baseURI . $endpoint, [
				'headers' => [
					'Authorization' => 'Bearer ' . $user->access,
					'Content-Type' => 'application/x-www-form-urlencoded',
					'Accept' => 'application/json'
				],
				'http_errors' => $showHttpFails
			]);
			return $response;
		} catch (\Exception $e) {
			abort(500, "Error with request: " . $e->getMessage());
			return false;
		}
	}
}
